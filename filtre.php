<?php include('partials/header.php') ?>

<?php include('partials/menu_offcanvas.php') ?>

<?php include('partials/preheader.php') ?>

<?php include('partials/nav.php') ?>

<div id="contenu" class="container-fluid py-4">
    <div class="row">

        <?php include('partials/filtre.php') ?>

        <?php include('partials/resultSearch.php') ?>

    </div>
</div>

<?php include('partials/prefooter.php') ?>

<?php include('partials/footer.php') ?>

<?php include('partials/script.php') ?>

<?php include('partials/postfooter.php') ?>
