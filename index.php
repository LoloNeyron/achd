<?php include('partials/header.php') ?>

<?php include('partials/menu_offcanvas.php') ?>

<?php include('partials/preheader.php') ?>

<?php include('partials/nav.php') ?>

<div id="contenu" class="container-fluid py-4">
    <div class="row">

        <?php include('partials/mainSlider.php') ?>

<!--        --><?php //include('partials/mainNav.php') ?>

        <?php include('partials/text.php') ?>

        <?php include('partials/cta.php') ?>

        <?php include('partials/sliderPromo.php') ?>

        <?php include('partials/actu.php') ?>

        <?php include('partials/form.php') ?>

        <?php include('partials/otherProducts.php') ?>

    </div>
</div>

<?php include('partials/prefooter.php') ?>

<?php include('partials/footer.php') ?>

<?php include('partials/script.php') ?>

<?php include('partials/postfooter.php') ?>

