<div class="container">
    <div class="row">
        <div class="col-12 py-4">
            <a href="#">
                <i class="fas fa-caret-left"></i> Liste des produits
            </a>
        </div>
        <div class="col-sm-12 col-md-7 col-lg-7">
            <h2 class="h3 bolder">images et vidéos</h2>
            <img src="https://via.placeholder.com/468x300?text=Slider+here" alt="" class="img-fluid ">
        </div>
        <div class="col-sm-12 col-md-5 col-lg-5">
            <h1 class="h1 bolder">SK08-1E</h1>
            <h3 class="h0 bolder">-</h3>
            <h2 class="h3 bolder">Mini excavators</h2>
            <h4 class="h4 pt-5 bolder">Product specifications SK08-1E</h4>
            <br>
            <p class="bolder">
                Engine rated power:
            </p>
            <p>
                7.7 kW / 2,400 min-1 (ISO9249)
            </p>
            <p class="bolder">
                Bucket capacity:
            </p>
            <p>
                0.022 m3
            </p>
            <br>
            <button class="btn btn-pc">
                <h3 class="h2 bolder capitalize">telecharger PDF</h3>
            </button>
        </div>
        <div class="col-12 py-3">
            <hr>
        </div>
        <div class="col-12">
            <h2 class="h1">Spécification du produit</h2>
            <br>
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <h3 class="h4"><i class="fab fa-searchengin color-primary"></i> ENGINE</h3>
                    <br>
                    <div class="row">
                        <div class="col-6">
                            <p>Model :</p>
                            <p>Type :</p>
                            <p>Fuel Tank :</p>
                            <p>Displacement :</p>
                            <p>Rated power output :</p>
                        </div>
                        <div class="col-6">
                            <p>Yanmar 2TE67L-BV3</p>
                            <p>Vertical type, Water-cooled, 4-cycle, 2-cylinder</p>
                            <p>9.7 L</p>
                            <p>0.507 L</p>
                            <p>	7.7 kW / 2,400 min-1 (ISO9249)</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <h3 class="h4"><i class="fab fa-searchengin color-primary"></i> ENGINE</h3>
                    <br>
                    <div class="row">
                        <div class="col-6">
                            <p>Model :</p>
                            <p>Type :</p>
                            <p>Fuel Tank :</p>
                            <p>Displacement :</p>
                            <p>Rated power output :</p>
                        </div>
                        <div class="col-6">
                            <p>Yanmar 2TE67L-BV3</p>
                            <p>Vertical type, Water-cooled, 4-cycle, 2-cylinder</p>
                            <p>9.7 L</p>
                            <p>0.507 L</p>
                            <p>	7.7 kW / 2,400 min-1 (ISO9249)</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <h3 class="h4"><i class="fab fa-searchengin color-primary"></i> ENGINE</h3>
                    <br>
                    <div class="row">
                        <div class="col-6">
                            <p>Model :</p>
                            <p>Type :</p>
                            <p>Fuel Tank :</p>
                            <p>Displacement :</p>
                            <p>Rated power output :</p>
                        </div>
                        <div class="col-6">
                            <p>Yanmar 2TE67L-BV3</p>
                            <p>Vertical type, Water-cooled, 4-cycle, 2-cylinder</p>
                            <p>9.7 L</p>
                            <p>0.507 L</p>
                            <p>	7.7 kW / 2,400 min-1 (ISO9249)</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <h3 class="h4"><i class="fab fa-searchengin color-primary"></i> ENGINE</h3>
                    <br>
                    <div class="row">
                        <div class="col-6">
                            <p>Model :</p>
                            <p>Type :</p>
                            <p>Fuel Tank :</p>
                            <p>Displacement :</p>
                            <p>Rated power output :</p>
                        </div>
                        <div class="col-6">
                            <p>Yanmar 2TE67L-BV3</p>
                            <p>Vertical type, Water-cooled, 4-cycle, 2-cylinder</p>
                            <p>9.7 L</p>
                            <p>0.507 L</p>
                            <p>	7.7 kW / 2,400 min-1 (ISO9249)</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>