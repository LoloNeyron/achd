<a href="#" class="col-4 bg-secondary text-center p-5 call-to-action animate pointer">
    <span class="h0 white-circle">
        <i class="fas fa-hammer"></i>
    </span>
    <h3 class="pt-4">Hammer</h3>
</a>
<a href="#" class="col-4 bg-secondary text-center p-5 call-to-action animate pointer">
    <span class="h0 white-circle">
        <i class="fas fa-screwdriver"></i>
    </span>
    <h3 class="pt-4">Screwdriver</h3>
</a>
<a href="#" class="col-4 bg-secondary text-center p-5 call-to-action animate pointer">
    <span class="h0 white-circle">
        <i class="fas fa-fan"></i>
    </span>
    <h3 class="pt-4">Fan</h3>
</a>