<div class="col-12 bg-primary py-5">
    <div class="row">
        <div class="col-4">
            <h3 class="h2">Adresses</h3>
            <ul class="none-list-type">
                <li>
                    <a href="https://www.google.fr/maps/place/Le+Rh%C3%B4ne/@44.9488591,5.3630736,8z/data=!3m1!4b1!4m5!3m4!1s0x478ea0dd7b9764f9:0x3fdd1de4986ccd2d!8m2!3d45.6402905!4d5.6864994">A
                        C Heavy Duty</a></li>
            </ul>
        </div>
        <div class="col-4">
            <h3 class="h2">Navigation</h3>
            <ul class="none-list-type">
                <li><a href="">bla bla blabla</a></li>
                <li><a href="">bla bla blabla</a></li>
                <li><a href="">bla bla blabla</a></li>
                <li><a href="">bla bla blabla</a></li>
                <li><a href="">bla bla blabla</a></li>
                <li><a href="">bla bla blabla</a></li>
            </ul>
        </div>
        <div class="col-4">
            <h3 class="h2">Actus</h3>
            <ul class="none-list-type">
                <li><a href="">blabla bla bla</a></li>
                <li><a href="">blabla bla bla</a></li>
                <li><a href="">blabla bla bla</a></li>
                <li><a href="">blabla bla bla</a></li>
                <li><a href="">blabla bla bla</a></li>
                <li><a href="">blabla bla bla</a></li>
            </ul>
        </div>
    </div>
</div>