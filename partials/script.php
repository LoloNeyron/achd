<script>
    window.addEventListener('load', function () {
        AOS.init();

        jQuery.fn.visible = function () {
            return this.css('visibility', 'visible');
        };

        jQuery.fn.invisible = function () {
            return this.css('visibility', 'hidden');
        };

        jQuery.fn.visibilityToggle = function () {
            return this.css('visibility', function (i, visibility) {
                return (visibility == 'visible') ? 'hidden' : 'visible';
            });
        };

        $('.cardToNavigate').hover(function () {
            $('nav', this).visibilityToggle();
        })

        var menuIsOpen = false;
        $('.menuBtn').on('click', function () {
            if (menuIsOpen === false) {
                $('#offcanvas-menu').css('left', 0);
                menuIsOpen = true;
            } else {
                $('#offcanvas-menu').css('left', "-100vw");
                menuIsOpen = false;
            }
        })

        var lastScrollTop = 0;
        $(window).scroll(function (event) {
            var st = $(this).scrollTop();
            if (st > lastScrollTop) {
                $('#Filtrage').removeClass('sticky-top');
            } else {
                $('#Filtrage').addClass('sticky-top');
                console.log('up');
            }
            lastScrollTop = st;
        });

        // setTimeout to simulate the delay from a real page load
        setTimeout(lazyLoad, 0);

        function lazyLoad() {
            var card_images = document.querySelectorAll('.card-image');
            // loop over each card image
            card_images.forEach(function (card_image) {
                var image_url = card_image.getAttribute('data-image-full');
                var content_image = card_image.querySelector('img');

                // change the src of the content image to load the new high res photo
                content_image.src = image_url;

                // listen for load event when the new photo is finished loading
                content_image.addEventListener('load', function () {
                    // swap out the visible background image with the new fully downloaded photo
                    card_image.style.backgroundImage = 'url(' + image_url + ')';
                    // add a class to remove the blur filter to smoothly transition the image change
                    card_image.className = card_image.className + ' is-loaded';
                });
            });
        }
    })
</script>