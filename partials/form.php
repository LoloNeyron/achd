<div class="col-12 py-5">
    <h2 class="h1 text-center">Contactez nous</h2>
    <form>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Nom</label>
                <input type="email" class="form-control">
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Prénom</label>
                <input type="password" class="form-control">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Société</label>
                <input type="email" class="form-control">
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Siret</label>
                <input type="password" class="form-control">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Adresse</label>
                <input type="email" class="form-control">
            </div>
            <div class="form-group col-md-3">
                <label for="inputPassword4">code postale</label>
                <input type="password" class="form-control">
            </div>
            <div class="form-group col-md-3">
                <label for="inputPassword4">ville</label>
                <input type="password" class="form-control">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">E-mail</label>
                <input type="email" class="form-control">
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Téléphone</label>
                <input type="password" class="form-control">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="inputEmail4">Message</label>
                <textarea rows="5" class="form-control"></textarea>
            </div>
        </div>
        <div class="d-block text-center">
            <button type="submit" class="btn btn-primary">Envoyer</button>
        </div>
    </form>
</div>