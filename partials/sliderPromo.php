<div class="col-12">
    <div class="row bg-dark">
        <div class="col-12 text-center py-3">
            <h2 class="h1 capitalize bolder">Decouvrez la selection du moment</h2>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-4">
                    <div class="card-image" data-image-full="assets/img/E1.jpg"
                         style="background-image: url('assets/img/E1x1.jpg');">
                        <img class="img-fluid" src="assets/img/E1x1.jpg" alt="Eli DeFaria"/>
                    </div>
                    <h3 class="h2 capitalize text-center sousligne">Ausa vp70</h3>
                    <p class="text-center">Tracto-pelle</p>
                </div>
                <div class="col-4">
                    <div class="card-image" data-image-full="assets/img/E1.jpg"
                         style="background-image: url('assets/img/E1.jpg');">
                        <img class="img-fluid" src="assets/img/E1.jpg" alt="Eli DeFaria"/>
                    </div>
                    <h3 class="h2 capitalize text-center sousligne">Ausa vp70</h3>
                    <p class="text-center">Tracto-pelle</p>
                </div>
                <div class="col-4">
                    <div class="card-image" data-image-full="assets/img/E3.jpg"
                         style="background-image: url('assets/img/E3.jpg');">
                        <img class="img-fluid" src="assets/img/E3.jpg" alt="Eli DeFaria"/>
                    </div>
                    <h3 class="h2 capitalize text-center sousligne">Ausa vp70</h3>
                    <p class="text-center">Tracto-pelle</p>
                </div>
            </div>
        </div>
    </div>
</div>