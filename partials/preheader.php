<header class="container-fluid bg-primary">
    <div class="row pt-2">
        <div class="col-6 text-center">
            <p>Du lundi au vendredi 8h-12h et 13h30-18h</p>
        </div>
        <div class="col-6 text-center">
            <a href="tel:0478569955"><i class="fas fa-phone"></i> 04 78 56 99 55</a>
        </div>
    </div>
</header>