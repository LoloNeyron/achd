<nav class="navbar navbar-light bg-secondary shadow">
    <a class="navbar-brand">
        <img src="assets/img/logo-achd.svg" alt="" class="img-fluid" width="50" height="50">
        ACHD
    </a>
    <button class="btn btn-tier menuBtn">Menu</button>
    <form class="form-inline">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-pc my-2 my-sm-0" type="submit">Search</button>
    </form>
</nav>